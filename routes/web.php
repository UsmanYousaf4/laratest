<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/register', function () {
    return view('auth/register');
});

Route::get('/', function () {
    return view('auth/login');
})->middleware('guest');

Route::get('/home', function () {
    return view('frontend/index');
})->middleware('auth');

Route::get('/signs', function () {
    return view('frontend/signs');
})->middleware('auth');

Route::get('/add-sign', function () {
    return view('frontend/add-sign');
})->middleware('auth');


Auth::routes();
Route::post('submitform','AddformController@addforms');
Route::get('/signs','SignController@list');
Route::get('/signs','SignController@list')->name('signs')->middleware('auth');
Route::post('/deleteform','AddformController@deleteform');
