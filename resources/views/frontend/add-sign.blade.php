@extends('frontend.layouts.app')

@section('content')

 <!-- Begin Page Content -->
 <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">User Agreement Form</h1>
    
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary"></h6>
      </div>
    
      <div class="card-body">
      			         
        <form method="POST"  action="submitform" enctype="multipart/form-data">
          @csrf

          <h6 class="heading-small text-muted mb-4">Agreement Information</h6>

          <div class="pl-lg-4">
            <div class="form-group">
              <embed src="{{ url('/pdf/End%20User%20License%20Agreement%20Sample.pdf') }}" width="600" height="500" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">
            </div>
          </div>

                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Name</label>
                        <input type="text" name="name" class="form-control" value="{{ Auth::user()->name }}" readonly>
                      </div>
                    </div>                    
                  </div>                
                </div>		
           		
				
				
                <hr class="my-4" />
                <!-- Address -->
                <h6 class="heading-small text-muted mb-4">Test Image</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        
						<input type="file" class="form-control" placeholder="" name="filep"  accept="image/*" capture="camera" required>
						<input type="hidden" class="form-control" id="canimg" name="canimg" value="" >
							
                      </div>
                    </div>				
					</div>
                  
                </div>
				
				 <hr class="my-4" />
				<h6 class="heading-small text-muted mb-4">Signature</h6>
				 <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-md-6">
				<div id="signature-pad" class="m-signature-pad">
				  <div class="m-signature-pad--body">
					<canvas></canvas>
				  </div>
				</div>
				<button id="replace">Clear</button>
				</div>				
					</div>
                  
                </div>
				
               	
			
				 <hr class="my-4" />
                <!-- Description -->
                <h6 class="heading-small text-muted mb-4">Comment</h6>
                <div class="pl-lg-4">
				<div class="row">
                    <div class="col-md-6">
                  <div class="form-group">                   
                    <textarea rows="4" class="form-control" name="comment" placeholder="" required></textarea>
                  </div>
                </div>
			   </div>
			   </div>


				<div class="pl-lg-4">
				<div class="row">
                 <div class="col-md-6">
				
				<div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
              <div class="d-flex justify-content-between">
			    <a href="home" class="btn btn-sm btn-default float-left">Back</a>
                <button type="submit" id="submit" name="submit" class="btn btn-sm btn-info  mr-4">Send</button>                
              </div>
            </div>
			
			 </div>
			   </div>
			   </div>


  </form>

    </div>
   

	
  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

@endsection