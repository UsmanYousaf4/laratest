@extends('frontend.layouts.app')

@section('content')

 <!-- Begin Page Content -->
 <div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>      
    </div>

    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
    </div>

    <!-- Content Row -->
    <div class="row">

      <!-- Card Example -->
      <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
          <a href="{{ url('/add-sign') }}">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Manage </div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">User Agreement Form</div>
              </div>
              <div class="col-auto">
                <i class="fa fa-building fa-2x text-300"></i>
              </div>
            </div>
          </div>
        </a>
        </div>
      </div>

      <!-- Card Example -->
      <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
          <a href="{{ url('/signs') }}">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Manage</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">Signs Datatable</div>
              </div>
              <div class="col-auto">
                <i class="fa fa-edit fa-2x text-300"></i>
              </div>
            </div>
          </div>
        </a>
        </div>
      </div>


    </div>


    <!-- Content Row -->
    
  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

@endsection