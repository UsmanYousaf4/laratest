<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Deleteuser;

class Deleteusers extends Controller
{
    //
    function delete(Request $req) {
      //print_r($req->input());
        $deleteuser=Deleteuser::find($req->id);
        $deleteuser->delete();
        return redirect('/tables');
    }
}
