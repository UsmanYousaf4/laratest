<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Addform;
use Storage;
class AddformController extends Controller
{
    function addforms (Request $req) 
    {
      
	  //print_r($req->input());
      $date = date("Y/m/d");	 

	  $path = Storage::putFile('public/images', $req->file('filep'));	 
	  $path = str_replace("public","",$path);
	   
      $pattol = new Addform;
      $pattol->name = $req->name;
      $pattol->date = $date;
      $pattol->image = $path;
      $pattol->comment = $req->comment;
	  $pattol->text = $req->canimg;
      $pattol->save();
      return redirect()->route('signs')->with('update', 'Sign Record has been Added successfully!'); 
    }
	
	function deleteform(Request $req) {
        //print_r($req->input());
          $deleteform=Addform::find($req->id);
          $deleteform->delete();
          return redirect()->route('signs')->with('update', 'Record has been Deleted successfully!');
      } 

    function view() {
      return view('frontend/add-sign');
    }
}

