<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addform extends Model
{
    //
    protected $table = 'forms';
    public $timestamps =false;
}
